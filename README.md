# 2.02 Installer

Repository containing all files necessary to build the 2.02 NSIS installer.

## Instructions

First, install NSIS: https://nsis.sourceforge.io/Download

Next, pick the installer you wish to build. We have 3:
* 202-full.nsi

  Inculdes both widescreen and 4:3 patches, replay compatibility back to v3.0.0 and full music support

* 202-mini-ws.nsi

  Includes only latest widescreen (16:9) patch

* 202-mini-std.nsi 

  Includes only latest 4:3 patch
  
## Binary builds
The latest binary builds (.exe) are stored on a separate branch without history.