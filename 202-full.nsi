# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "RotWK 2.02 version 8.0.0"
OutFile "202_v8_full.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma
;SetCompressor /SOLID zlib
;~ SetCompress off 

; Customization of the license page (so it says readme rather than license)
!define MUI_PAGE_HEADER_TEXT "Read Me"
!define MUI_PAGE_HEADER_SUBTEXT "For new players"
!define MUI_LICENSEPAGE_TEXT_TOP "Please read the following information:"
!define MUI_LICENSEPAGE_TEXT_BOTTOM " . "
!define MUI_LICENSEPAGE_BUTTON "Next >"

; Customization of the finish page (checkbox for desktop schortcut)
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_CHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcuts"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

!define MUI_ICON "202icon.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcome.bmp"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


;;
; Helper function that removes the content of a directory
;;
!macro RemoveFilesAndSubDirs DIRECTORY
  !define Index_RemoveFilesAndSubDirs 'RemoveFilesAndSubDirs_{__LINE__}'

  Push $0
  Push $1
  Push $2

  StrCpy $2 "${DIRECTORY}"
  FindFirst $0 $1 "$2*.*"
${Index_RemoveFilesAndSubDirs}-loop:
  StrCmp $1 "" ${Index_RemoveFilesAndSubDirs}-done
  StrCmp $1 "." ${Index_RemoveFilesAndSubDirs}-next
  StrCmp $1 ".." ${Index_RemoveFilesAndSubDirs}-next
  IfFileExists "$2$1\*.*" ${Index_RemoveFilesAndSubDirs}-directory
  ; file
  Delete "$2$1"
  goto ${Index_RemoveFilesAndSubDirs}-next
${Index_RemoveFilesAndSubDirs}-directory:
  ; directory
  RMDir /r "$2$1"
${Index_RemoveFilesAndSubDirs}-next:
  FindNext $0 $1
  Goto ${Index_RemoveFilesAndSubDirs}-loop
${Index_RemoveFilesAndSubDirs}-done:

  FindClose $0

  Pop $2
  Pop $1
  Pop $0
  !undef Index_RemoveFilesAndSubDirs
!macroend

Var L1A
Var L1B
Var L2A
Var L2B
Var L3A
Var L3B
Var L6A
Var L6B
Var LEAF

;;
; Main component
;;
Section
	ReadRegStr $LEAF HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" UserDataLeafName
	DetailPrint "Your appdata directory is called: $LEAF"

	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	;Var /GLOBAL ForeignLang
	;StrCpy $ForeignLang 0

	DetailPrint "Determining language..."
		;Check if packaged bfme2 folder has not moved or sth
	${If} ${FileExists} "lang\English.big"
		DetailPrint "Your game language is English."
	${Else}
		DetailPrint "Your game language is not English."
		MessageBox MB_OK|MB_ICONEXCLAMATION "English language installation not found! By default, 2.02 ships with English language support only. Be aware that certain in-game texts such as the version tag in the Options menu will not be accurate. Visit www.gamereplays.org for foreign language support."
		;StrCpy $ForeignLang 1
	${EndIf}

	DetailPrint "Finding 2.01..."
	;Check if 2.01 exists
		${If} ${FileExists} "_patch201.big"
			DetailPrint "Good, 2.01 is present."
		${Else}
		DetailPrint "2.01 Not found! I will install it for you."
		File /r "Patch201\*.*"

		;MessageBox MB_OK|MB_ICONSTOP "2.01 files not found! Please install 2.01 first."
		;Abort
		${EndIf}
		
	; delete all .disabled files, they can only interfere
	; this should be safe since only 2.02 makes these type of files
	FindFirst $L1A $L1B "$INSTDIR\*.disabled"
		loop:
			StrCmp $L1B "" done
			Delete $L1B
			FindNext $L1A $L1B
			Goto loop
		done:
	FindClose $L1A
	
	; delete all asset files
	FindFirst $L2A $L2B "$INSTDIR\asset*"
		loop2:
			StrCmp $L2B "" done2
			Delete $L2B
			FindNext $L2A $L2B
			Goto loop2
		done2:
	FindClose $L2A

	; remove the old lobbymusic mods
	FindFirst $L3A $L3B "$INSTDIR\___lobbymusic-*"
		loop3:
			StrCmp $L3B "" done3
			Delete $L3B
			FindNext $L3A $L3B
			Goto loop3
		done3:
	FindClose $L3A
	
	; delete beta related files
	FindFirst $L6A $L6B "$INSTDIR\!*"
		loop6:
			StrCmp $L6B "" done6
			Delete $L6B
			FindNext $L6A $L6B
			Goto loop6
		done6:
	FindClose $L6A

	!insertmacro RemoveFilesAndSubDirs "$INSTDIR\launcher_releases\"  ;remove all launcher recipes - we ship new ones

	Delete "$INSTDIR\######unofficialpatch202_v4.0.0.big"  ;we ship this file in the installer as a .disabled. Last thing we want is this file screwing us over

	Delete "$INSTDIR\Unofficial202Launcher.exe"  ; delete old launcher
	Delete "$INSTDIR\Uninstall.exe"  ; delete old uninstaller

	; some misc risidual files of previous 2.02 install
	Delete "$INSTDIR\_U202Changelog.txt"
	Delete "$INSTDIR\__launcherlocalization.txt"
	Delete "$INSTDIR\_launchercontrol.ini"
	Delete "$INSTDIR\_u202info.ini"
	Delete "$INSTDIR\new_launcher.exe"
	Delete "$INSTDIR\new_launcher_source.zip"

	; use the entire directory and the music-related files
	File /r "both\*.*"
	File /r "Replay Parser files"
	File /r "Music\*.*"
	
	;For non-Windows build environments, use this:
	;File /r "both/*"

	; Write the options.ini file for Win8 and Win10 users
	SetShellVarContext current
	CreateDirectory "$APPDATA\$LEAF\"
	CreateDirectory "$APPDATA\$LEAF\Replays"

	${IfNot} ${FileExists} "$APPDATA\$LEAF\*.*"
		DetailPrint "Could not create $APPDATA\$LEAF\"
		MessageBox MB_OK|MB_ICONEXCLAMATION|mb_SetForeground "Can not create folder $APPDATA\$LEAF\... $\n$\nYour game will likely freeze upon launching it. Ask the 2.02 tech support team for help."
	${Else}
		DetailPrint "AppData folder was found. Installing Options.ini."
		SetOutPath "$APPDATA\$LEAF"
		SetOverwrite off
		File "Options.ini"
		DetailPrint "Options.ini installed."
		SetOutPath "$APPDATA\$LEAF\Replays"
		File "Last Replay.BfME2Replay"
		DetailPrint "Dummy Last Replay file added for the Replay Parser."
		SetOverwrite on
	${EndIf}
	
	SetShellVarContext all

	; Reset back to regular install directory
	SetOutPath $INSTDIR

	writeUninstaller "$INSTDIR\uninstall02.exe"

	; Add the uninstaller to "add/remove programs" in Windows
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"DisplayName" "Rise of the Witch-King 2.02"

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"Publisher" "RotWK 2.02 Team"

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"UninstallString" "$\"$INSTDIR\uninstall02.exe$\""
SectionEnd



;;
; Normal screen component
;;
Section "Widescreen" secws
	SetOutPath $INSTDIR

	File /r "FullWs\*.*"
	;For non-Windows build environments, use this:
	;File /r "v7ws/*"

SectionEnd

;;
; Wide screen component
;;
Section /o "4:3 monitor" secstd
	SetOutPath $INSTDIR

	File /r "FullStd\*.*"
	;For non-Windows build environments, use this:
	;File /r "v7std/*"

SectionEnd


;;
; Music component
;;
;Section "Additional music" secmusic
;	SetOutPath $INSTDIR
;	delete "$INSTDIR\!vanilla202music.big"
;	File /r "Music\*.*"

;	;For non-Windows build environments, use this:
;	;File /r "Music/*"

;SectionEnd


Function .onInit
StrCpy $1 ${secws} ;The default
FunctionEnd

Function .onSelChange
!insertmacro StartRadioButtons $1
    !insertmacro RadioButton ${secws}
    !insertmacro RadioButton ${secstd}
!insertmacro EndRadioButtons
FunctionEnd

;;
; Make a desktop shortcut if the user asked for it
;;
Function finishpageaction
	CreateShortcut "$desktop\2.02 Switcher.lnk" "$INSTDIR\202_launcher.exe"
	SetOutPath "$INSTDIR\Replay Parser files\"
	CreateShortcut "$desktop\2.02 Replay Parser.lnk" "$INSTDIR\Replay Parser files\UI.exe"
	SetOutPath $INSTDIR
FunctionEnd



;;
; This spawns the confirmation window when uninstalling
;;
function un.onInit
	; Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove RotWK 2.02 version 8.0.0?" IDOK next
		Abort
	next:

functionEnd

;;
; Uninstaller section
;;
section "uninstall"
	SetOutPath "$INSTDIR"

	; first, we delete the dat files, whichever version tey may be
	Delete "$INSTDIR\game.dat"
	Delete "$INSTDIR\game.other"
	Delete "$INSTDIR\asset.dat"
	Delete "$INSTDIR\asset.other"

	; then, we manually install the old asset and game dats
	File "both\game.other"
	File "both\asset.other"

	; And of course rename them to the proper name
	CopyFiles "$INSTDIR\game.other" "$INSTDIR\game.dat"
	Delete "$INSTDIR\game.other"
	CopyFiles "$INSTDIR\asset.other" "$INSTDIR\asset.dat"
	Delete "$INSTDIR\asset.other"

	; Remove Start Menu launcher
	Delete "$desktop\2.02 Switcher.lnk"
	Delete "$desktop\2.02 Replay Parser.lnk"
	
	; delete all .disabled files, they can only interfere
	; this should be safe since only 2.02 makes these type of files
	FindFirst $0 $1 "$INSTDIR\*.disabled"
		loop:
			StrCmp $1 "" done
			Delete $1
			FindNext $0 $1
			Goto loop
		done:
	FindClose $0


	; delete all 202 related files
	FindFirst $2 $3 "$INSTDIR\*202*"
		loop3:
			StrCmp $3 "" done3
			Delete $3
			FindNext $2 $3
			Goto loop3
		done3:
	FindClose $2


	; delete all lang files in the ugliest way possible
	SetShellVarContext current

	${For} $R1 1 25 ; make sure the rightmost number exceeds the amount of lang files
		Delete "$INSTDIR\lang\*202*"
	${Next}

	SetShellVarContext all


	!insertmacro RemoveFilesAndSubDirs "$INSTDIR\launcher_releases\"
	RMDir /r "$INSTDIR\Replay Parser files\"

	; Delete launcher and related files
	Delete "$INSTDIR\202_launcher.exe"
	Delete "$INSTDIR\bg.png"
	Delete "$INSTDIR\202_launcher_source.zip"
	
	rmDir "$INSTDIR\launcher_releases"

	; Delete the uninstaller key for "add/remove programs"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK"

	; Always delete uninstaller as the last action
	Delete "$INSTDIR\uninstall02.exe"
sectionEnd

;;
; Descriptions that show up on the components page
;;
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	;!insertmacro MUI_DESCRIPTION_TEXT ${secmusic} "This component contains the improved music pack which offers a more varied, larger selection of LoTR music."
	!insertmacro MUI_DESCRIPTION_TEXT ${secstd} "This component contains the patch for old monitors (4:3 resolution)."
	!insertmacro MUI_DESCRIPTION_TEXT ${secws} "This component contains the patch for widescreen monitors (16:9 resolution)."


!insertmacro MUI_FUNCTION_DESCRIPTION_END

