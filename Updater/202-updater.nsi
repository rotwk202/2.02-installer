# Modern UI example script
!include MUI.nsh
!include sections.nsh
!include LogicLib.nsh

Name "RotWK 2.02 version 8.5.0"
OutFile "202_v850_updater.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

; Use this compress setting instead of the above while developing to shorten build times
;~ SetCompress off


; Customization of the license page (so it says readme rather than license)
!define MUI_PAGE_HEADER_TEXT "Read Me"
!define MUI_PAGE_HEADER_SUBTEXT "For new players"
!define MUI_LICENSEPAGE_TEXT_TOP "Please read the following information:"
!define MUI_LICENSEPAGE_TEXT_BOTTOM " . "
!define MUI_LICENSEPAGE_BUTTON "Next >"

; Customization of the finish page (checkbox for desktop schortcut)
; !define MUI_FINISHPAGE_SHOWREADME ""
; !define MUI_FINISHPAGE_SHOWREADME_CHECKED
; !define MUI_FINISHPAGE_SHOWREADME_TEXT "Create League Tool Shortcut"
; !define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

!define MUI_ICON "..\202icon.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "..\welcome.bmp"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


Var LEAF
Var wsdetector

;;
; Main component
;;
Section
	ReadRegStr $LEAF HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" UserDataLeafName
	DetailPrint "Your appdata directory is called: $LEAF"

	SetOutPath $INSTDIR

	DetailPrint "Determining language..."
	
	;Check language. If not English, show warning about incorrect in-game text.
	${If} ${FileExists} "lang\English.big"
		DetailPrint "Your game language is English."
	${Else}
		DetailPrint "Your game language is not English."
		MessageBox MB_OK|MB_ICONEXCLAMATION "English language installation not found! By default, 2.02 ships with English language support only. Be aware that certain in-game texts such as the version tag in the Options menu will not be accurate. Visit www.gamereplays.org for foreign language support."
	${EndIf}


	DetailPrint "Determining install type..."
	;Check if v8 is installed
	${If} ${FileExists} "##########202_v8.0.0.big"
	${OrIf} ${FileExists} "##########202_v8.0.0.disabled"
		DetailPrint "Good, 2.02 v8.0.0 is present."
	${Else}
		DetailPrint "2.02 v8.0.0 not found!"
		MessageBox MB_OK|MB_ICONSTOP "2.02 v8.0.0 not found! Please install 2.02 v8.0.0 first before using this updater."
		Abort
	${EndIf}
	
	;Check if v8.2 is installed. If not, armorfix game.dat is not yet there so we should add it.
	;The only reason we do this here is because what if someone tries to upgrade 8.3 to 8.5? 
	;They already have armorfix so no need to add it twice.
	${If} ${FileExists} "############202_v8.2.0.big"
	${OrIf} ${FileExists} "############202_v8.2.0.disabled"
		DetailPrint "8.2.0 already here"
	${Else}
		DetailPrint "2.02 v8.2.0 not found, installing armorfix."
		; move old game.dat and install new armorfix one
		CopyFiles "game.dat" "game820.dat"
		File "game.dat"
	${EndIf}

	;Check if v8.4 is installed. If not, we assume we're upgrading from 8.0 to 8.5 and just 
	; add everything.
	;Here we don't care if we needlessly add 8.1, 8.2 .big files cause they overwrite instead 
	; of adding another file like the armorfix game.dat above.
	${If} ${FileExists} "##############202_v8.4.0.big"
	${OrIf} ${FileExists} "##############202_v8.4.0.disabled"
		DetailPrint "8.4.0 already here"
	${Else}
		DetailPrint "8.4.0 not found, installing 8.4.0 specific files."
		
		; move old asset.dat and install newest one 
		; version 8.0 - 8.3 use same asset.dat
		; version 8.4 - 8.5 use this new one
		CopyFiles "asset.dat" "asset830c.dat"
		File "asset.dat"

		File "###########202_v8.0.1.big"
		File "############202_v8.2.0.big"
		File "#############202_v8.3.0.big"

		SetOutPath "$INSTDIR\launcher_releases"
		File "launcher_releases\2.02 v8.2.0.ini"
		File "launcher_releases\2.02 v8.3.0.ini"

		SetOutPath "$INSTDIR\data\"
		File "leaf-icon.ico"
		SetOutPath "$INSTDIR"

		Delete "$INSTDIR\Replay Parser files\*"
		RMDir "$INSTDIR\Replay Parser files\"
		Delete "$desktop\2.02 Replay Parser.lnk"

	${EndIf}

	;Check if full or mini
	${If} ${FileExists} "######202_v5.0.1.big"  ; this file is only on full installs
	${OrIf} ${FileExists} "######202_v5.0.1.disabled"
		DetailPrint "Full version detected"

		${If} ${FileExists} "######202_v5.0.1.big"
			StrCpy $wsdetector "######202_v5.0.1.big"
		${Else}
			StrCpy $wsdetector "######202_v5.0.1.disabled"
		${EndIf}

		FileOpen $0 "$INSTDIR\$wsdetector" r

		; Read the fifth byte of 5.0.1 file -- it's different between WS and STD
		${For} $R1 1 5
			FileReadByte $0 $1
		${Next}
		FileClose $0

		; MessageBox MB_OK|MB_ICONEXCLAMATION "Magic value is $1"

		${If} $1 == 202
			DetailPrint "4:3 installation"
			; MessageBox MB_OK|MB_ICONEXCLAMATION "This is standard screen"
			File /r "FullStd\*.*"
		${Else}
			DetailPrint "16:9 installation"
			; MessageBox MB_OK|MB_ICONEXCLAMATION "This is wide screen"
			File /r "FullWs\*.*"
		${EndIf}
	${Else}
		DetailPrint "Mini installation"
		; MessageBox MB_OK|MB_ICONEXCLAMATION "This is mini"
		File /r "Mini\*.*"
	${EndIf}

	; Install all language files
	File /r "lang"
	
	; Always overwrite launcher with newest version
	File "202_launcher.exe"

SectionEnd

;;
; Make a desktop shortcut if the user asked for it
;;
;Function finishpageaction
;	CreateShortcut "$desktop\RotWK League Tool.lnk" "$INSTDIR\202_launcher.exe" "repparser" "$INSTDIR\data\leaf-icon.ico"
;	;~ SetOutPath "$INSTDIR\Replay Parser files\"
;	;~ CreateShortcut "$desktop\2.02 Replay Parser.lnk" "$INSTDIR\Replay Parser files\UI.exe"
;	SetOutPath $INSTDIR
;FunctionEnd
